<?php

use Inertia\Inertia;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {

        $directorios_del_cliente = Storage::directories("/public/1052381109_hugo_sas");

        // creas un array vacío para ir llenándolo con los elementos año/mes/archivos
        $tree_array = [];

        // iteras sobre los directorios obtenidos
        foreach ($directorios_del_cliente as $directorio_del_ano) {
            // haciendo un explode, separas el string en un array con cada directorio.
            $temp_array = explode('/', $directorio_del_ano);
            // obtienes el último elemento (el año).
            $year = end($temp_array);
            // Obtienes los subdirectorios que están dentro del directorio del año
            $subdirectorios_del_ano = Storage::directories("/public/1052381109_hugo_sas/$year");
            foreach ($subdirectorios_del_ano as $directorio_del_mes) {
                $temp_array = explode('/', $directorio_del_mes);
                // obtienes el último elemento (el mes).
                $month = end($temp_array);
                // Obtienes los archivos que están dentro del directorio del mes
                $archivos_del_mes = Storage::files($directorio_del_mes);
                foreach ($archivos_del_mes as $archivo_del_mes) {
                    $temp_array = explode('/', $archivo_del_mes);
                    // obtienes el último elemento (el nombre del archivo).
                    $filename = end($temp_array);
                    // obtienes la url que corresponde a ese archivo.
                    $url = Storage::url($archivo_del_mes);
                    // guardas en tu array el nombre y la url del archivo,
                    // haciendo que el array sea multidimensional por año y mes.
                    $tree_array[$year][$month][] = [
                        'filename' => $filename,
                        'url' => $url
                    ];
                }
            }
        }

        return Inertia::render('Dashboard',[
            'tree_array' => $tree_array
        ]
    );
    })->name('dashboard');
});
